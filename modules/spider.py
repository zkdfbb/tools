#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: zhangkai
Email: zhangkai@cmcm.com
Last modified: 2018-01-05 17:21:17
'''
import asyncio
import collections
import inspect
import sys
import types
from argparse import ArgumentParser
from asyncio.locks import Lock
from concurrent.futures._base import CancelledError
from functools import partial
from importlib import import_module
from signal import SIGINT
from signal import SIGTERM
from urllib.parse import quote

from apscheduler.schedulers.asyncio import AsyncIOScheduler
from tornado.queues import Queue
from utils import Logger
from utils import Redis
from utils import Request


def sched(trigger, **trigger_kwargs):
    def wrapper(func):
        func._trigger = trigger
        func._trigger_kwargs = trigger_kwargs
        return func
    return wrapper


class BaseChecker:

    def __init__(self, retries=3):
        self.retries = retries
        self.lock = Lock()

    async def __call__(self, url):
        with await self.lock:
            if self.check('succeed', url) or self.check('crawling', url):
                return False
            if self.incr(url) < self.retries:
                self.add('crawling', url)
                return True
            else:
                self.add('failed', url)
                return False


class MemoryChecker(BaseChecker):

    def __init__(self, retries=3):
        super().__init__(retries=retries)
        self.cache = collections.defaultdict(int)
        self.storage = collections.defaultdict(set)

    def incr(self, url):
        self.cache[url] += 1
        return self.cache[url]

    def add(self, name, url):
        self.storage[name].add(url)

    def remove(self, name, url):
        if url in self.storage[name]:
            self.storage[name].remove(url)

    def check(self, name, url):
        return url in self.storage[name]


class RedisChecker(BaseChecker):

    def __init__(self, retries=3, prefix='spider'):
        super().__init__(retries=retries)
        self.prefix = prefix
        self.rd = Redis()

    def incr(self, url):
        return self.rd.hincrby(f'{self.prefix}_count', url, 1)

    def add(self, name, url):
        self.rd.sadd(f'{self.prefix}_{name}', url)

    def remove(self, name, url):
        self.rd.srem(f'{self.prefix}_{name}', url)

    def check(self, name, url):
        return self.rd.sismember(f'{self.prefix}_{name}', url)


class SpiderMeta(type):

    def __new__(cls, name, bases, attrs):
        sched_jobs = []
        for job in attrs.values():
            if inspect.isfunction(job) and getattr(job, '_trigger', None):
                sched_jobs.append(job)
        newcls = type.__new__(cls, name, bases, attrs)
        newcls._sched_jobs = sched_jobs
        return newcls


class Spider(metaclass=SpiderMeta):

    _sched_jobs = []
    urls = []

    def __init__(self,
                 workers=50,
                 checker=None,
                 prefix='spider',
                 retries=5,
                 timeout=30,
                 splash=False,
                 splash_url='http://localhost:8050/render.html'):
        self.workers = workers
        self.splash = splash
        self.splash_url = splash_url
        self.logger = Logger(name='tornado.application')
        self.http = Request(lib='tornado', max_clients=workers, timeout=timeout)
        self.queue = Queue()
        if checker == 'RedisChecker':
            self.checker = RedisChecker(retries=retries, prefix=prefix)
        elif checker:
            self.checker = getattr(sys.modules[__name__], checker)(retries=retries)
        else:
            self.checker = None

        self.sched = AsyncIOScheduler()
        self.sched.start()
        self.loop = asyncio.get_event_loop()

    async def crawl(self, url, callback, *args, **kwargs):
        if self.checker is None or await self.checker(url) or kwargs.get('method') in ['POST', 'PUT']:
            await self.queue.put((url, callback, args, kwargs))

    async def parse(self, resp):
        self.logger.info(f'{resp.code}: {resp.url}')

    async def producer(self):
        for url in self.urls:
            await self.crawl(url, self.parse)

    async def consumer(self):
        while True:
            try:
                url, callback, args, kwargs = await self.queue.get()
                if self.checker is not None:
                    self.checker.remove('crawling', url)

                codes = kwargs.pop('codes', [])
                if self.splash:
                    _url = f'{self.splash_url}?url={quote(url)}&wait=0.5'
                    resp = await self.http.request(_url, **kwargs)
                else:
                    resp = await self.http.request(url, **kwargs)

                self.logger.info(f'queue: {self.queue.qsize()}, url: {url} {resp.code} {resp.reason}')
                if (codes and resp.code in codes) or (not codes and 200 <= resp.code < 300):
                    if self.checker is not None:
                        self.checker.add('succeed', url)
                    doc = callback(resp, *args)
                    if isinstance(doc, types.CoroutineType):
                        doc = await doc
                elif codes and resp.code == 599:
                    kwargs['codes'] = codes
                    await self.crawl(url, callback, *args, **kwargs)
                elif not codes:
                    if 400 <= resp.code < 500:
                        if self.checker is not None:
                            self.checker.add('failed', url)
                    else:
                        await self.crawl(url, callback, *args, **kwargs)
            except CancelledError as e:
                return self.logger.error(f'Cancelled consumer')
            except Exception as e:
                self.logger.exception(f'{url}: {e}')
                self.queue.task_done()
            else:
                self.queue.task_done()

    async def shutdown(self, sig):
        self.logger.warning('caught {0}'.format(sig.name))
        tasks = [task for task in asyncio.Task.all_tasks() if task is not
                 asyncio.tasks.Task.current_task()]
        list(map(lambda task: task.cancel(), tasks))
        results = await asyncio.gather(*tasks, return_exceptions=True)
        self.logger.info(f'finished awaiting cancelled tasks: {len(results)}')
        self.loop.stop()

    async def run(self):
        try:
            await self.producer()
            await self.queue.join()
        except CancelledError as e:
            return self.logger.error(f'Cancelled producer')

    def start(self):
        for _ in range(self.workers):
            self.loop.create_task(self.consumer())

        for sig in (SIGINT, SIGTERM):
            self.loop.add_signal_handler(sig, partial(self.loop.create_task, self.shutdown(sig)))

        if self._sched_jobs:
            if self.checker is not None:
                self.logger.warning('checker is not None')
            for func in self._sched_jobs:
                function = func.__get__(self, self.__class__)
                self.sched.add_job(function, func._trigger, **func._trigger_kwargs)
                self.loop.create_task(function())
            self.loop.run_forever()
        else:
            self.loop.run_until_complete(self.run())


def main():
    parser = ArgumentParser(prog='spider')
    parser.add_argument('--timeout', type=int, default=30)
    parser.add_argument('--workers', type=int, default=20)
    parser.add_argument('--checker', type=str, default=None)
    parser.add_argument('--retries', type=int, default=3)
    parser.add_argument('--prefix', type=str, default='spider')
    parser.add_argument('--splash', action='store_true', default=False)
    parser.add_argument('--splash_url', type=str, default='http://localhost:8050/render.html')
    args, argv = parser.parse_known_args()
    logger = Logger()
    kwargs = args.__dict__
    logger.info(kwargs)
    if len(argv) >= 1:
        module_name, app_name = argv[0].split('.')
        module = import_module(module_name)
        app = getattr(module, app_name)(**kwargs)
    else:
        app = Spider(**kwargs)
    app.start()


if __name__ == '__main__':
    main()
