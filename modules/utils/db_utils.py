#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: zhangkai
Email: zhangkai@cmcm.com
Last modified: 2018-01-05 11:24:17
'''
import asyncio
import os
from urllib.parse import quote_plus

import aioredis
import pymongo
import redis
from motor import core
from motor.frameworks import asyncio as asyncio_framework
from motor.metaprogramming import create_class_with_framework

from .utils import DictWrapper

__all__ = ['Mongo', 'MongoClient', 'Redis', 'AioRedis']


class Cursor(pymongo.cursor.Cursor):

    def __next__(self, *args, **kwargs):
        doc = super(Cursor, self).next(*args, **kwargs)
        return DictWrapper(doc)

    next = __next__


class Collection(pymongo.collection.Collection):

    def find(self, *args, **kwargs):
        kwargs.update({'no_cursor_timeout': True})
        return Cursor(self, *args, **kwargs)

    def find_one(self, *args, **kwargs):
        doc = super(Collection, self).find_one(*args, **kwargs)
        return DictWrapper(doc)

    def find_one_and_update(self, *args, **kwargs):
        doc = super(Collection, self).find_one_and_update(*args, **kwargs)
        return DictWrapper(doc)

    def find_one_and_delete(self, *args, **kwargs):
        doc = super(Collection, self).find_one_and_delete(*args, **kwargs)
        return DictWrapper(doc)


class Database(pymongo.database.Database):

    def __getitem__(self, name):
        return Collection(self, name)

    def get_id(self, collection):
        ret = self.ids.find_one_and_update({'_id': collection},
                                           {'$inc': {'seq': 1}},
                                           upsert=True,
                                           projection={'seq': True, '_id': False},
                                           return_document=True)
        return ret['seq']


class MongoClient(pymongo.MongoClient):

    def __init__(self, host='localhost', port=27017, user=None, pwd=None, **kwargs):
        if os.environ.get("env") != "debug":
            host = os.environ.get("MONGO_HOST", "localhost")
            port = int(os.environ.get("MONGO_PORT", 27017))
            user = os.environ.get("MONGO_USER", None)
            pwd = os.environ.get("MONGO_PWD", None)

        if user and pwd:
            uri = f"mongodb://{quote_plus(user)}:{quote_plus(pwd)}@{host}:{port}"
        else:
            uri = f"mongodb://{host}:{port}"
        super(MongoClient, self).__init__(uri, **kwargs)

    def __getitem__(self, name):
        return Database(self, name)

    def __getattr__(self, name):
        return Database(self, name)


class Mongo(Database):

    def __init__(self, db=None, **kwargs):
        client = MongoClient(**kwargs)
        db = db or os.environ.get("MONGO_DB", "test")
        super(Mongo, self).__init__(client, db)


class AgnosticCursor(core.AgnosticCursor):
    __delegate_class__ = Cursor


class AgnosticCollection(core.AgnosticCollection):
    __delegate_class__ = Collection


class AgnosticDatabase(core.AgnosticDatabase):
    __delegate_class__ = Database


class AgnosticClient(core.AgnosticClient):
    __delegate_class__ = MongoClient


def create_asyncio_class(cls):
    asyncio_framework.CLASS_PREFIX = ''
    return create_class_with_framework(cls, asyncio_framework, 'db_utils')


MotorClient = create_asyncio_class(AgnosticClient)
MotorDatabase = create_asyncio_class(AgnosticDatabase)
MotorCollection = create_asyncio_class(AgnosticCollection)
MotorCursor = create_asyncio_class(AgnosticCursor)


class Redis(redis.StrictRedis):

    def __init__(self, host='localhost', port=6379, password=None, db=None, **kwargs):
        if os.environ.get("env") != "debug":
            host = os.environ.get("REDIS_HOST", "localhost")
            port = int(os.environ.get("REDIS_PORT", 6379))
            password = os.environ.get("REDIS_PWD", None)
            db = int(os.environ.get("REDIS_DB", 0))

        kwargs.setdefault('decode_responses', True)
        pool = redis.ConnectionPool(db=db, host=host, port=port, password=password, **kwargs)
        super().__init__(connection_pool=pool)

    def clear(self, pattern='*'):
        if pattern == '*':
            self.flushdb()
        else:
            keys = [x for x in self.scan_iter(pattern)]
            if keys:
                self.delete(*keys)


class AioRedis(aioredis.Redis):

    def __init__(self, host='localhost', port=6379, password=None, db=None, decode_responses=True, **kwargs):
        if os.environ.get("env") != "debug":
            host = os.environ.get("REDIS_HOST", "localhost")
            port = int(os.environ.get("REDIS_PORT", 6379))
            password = os.environ.get("REDIS_PWD", None)
            db = int(os.environ.get("REDIS_DB", 0))

        encoding = 'utf8' if decode_responses else None
        kwargs.setdefault('encoding', encoding)
        kwargs.setdefault('loop', asyncio.get_event_loop())
        pool = aioredis.create_pool((host, port), db=db, password=password, **kwargs)
        super().__init__(kwargs['loop'].run_until_complete(pool))

    async def set(self, name, value, ex=None, px=None, nx=False):
        if ex is not None:
            await super().setex(name, ex, value)
        elif px is not None:
            await super().psetex(name, px, value)
        elif nx:
            await super().setnx(name, value)
        else:
            await super().set(name, value)

    async def clear(self, pattern='*'):
        if pattern == '*':
            await self.flushdb()
        else:
            keys = []
            async for key in self.iscan(match=pattern):
                keys.append(key)
            if keys:
                await self.delete(*keys)


# import torndb
# class Mysql(torndb.Connection):
#
#     def __init__(self, db='pua', **kwargs):
#         kwargs.setdefault('time_zone', '+08:00')
#         kwargs['database'] = db
#         super(Mysql, self).__init__(**kwargs)
