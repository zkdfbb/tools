#!/usr/bin/env bash
# -*- coding:utf-8 -*-

# set -x
basepath=$(cd `dirname $0`/..; pwd)
cd $basepath
mkdir -p opt/bin opt/lib opt/include tmp runtime script/logs
optpath=$(cd opt; pwd)
tmppath=$(cd tmp; pwd)
runpath=$(cd runtime; pwd)
confpath=$(cd conf; pwd)
config=$basepath/script/config.ini

SHELL="$(ps c -p "$PPID" -o 'ucomm=' 2>/dev/null || true)"
SHELL="${SHELL##-}"
SHELL="${SHELL%% *}"
SHELL="$(basename "${SHELL:-$SHELL}")"

case "$SHELL" in
    bash ) profile="$HOME/.bashrc" ;;
    zsh ) profile="$HOME/.zshrc" ;;
    ksh ) profile="$HOME/.profile" ;;
    fish ) profile="$HOME/.config/fish/config.fish" ;;
    * ) echo "please set your profile !!!"; exit 1 ;;
esac

OS=$(echo `uname -s` | tr 'A-Z' 'a-z')
ARCH=$(echo `uname -m` | tr 'A-Z' 'a-z')

[[ "$ARCH" == "aarch64" ]] && ARCH=arm64
[[ "$ARCH" == "x86_64" ]] && ARCH=amd64

function decompress(){
    filename=$1
    old_dirs=`ls | awk '{print $NF}'`
    if [[ "$filename" =~ (tar.gz|tar.bz2|tar.xz|tgz)$ ]] || `file -i $filename | egrep 'x-gzip|x-bzip2|x-xz' &>/dev/null`; then
        tar xf $filename
    elif [[ "$filename" =~ (gz)$ ]] || `file -i $filename | egrep "application/gzip" &>/dev/null`; then
        gunzip $filename
    elif [[ "$filename" =~ (zip)$ ]] || `file -i $filename | egrep "zip" &>/dev/null`; then
        unzip $filename
    else
        return 1
    fi
    new_dirs=`ls | awk '{print $NF}'`
    dirname=`echo -e "$new_dirs\n$old_dirs" | sort | uniq -u`
}

function download(){
    cd $basepath/tmp
    url=$1
    filename=`basename "$url"`
    git=false
    binary=false
    shift 1
    # first ":" means ignore error
    # ":" after each chatacter means parameter is needed
    while getopts ":f:d:p:gb" opt
    do
        case $opt in
            g) git=true;;
            b) binary=true;;
            f) filename=$OPTARG;;
            d) dirname=$OPTARG;;
            p) path=$OPTARG;;
            ?) echo "wrong parameter $opt";;
        esac
    done

    if [[ "$path" != "" ]]; then
        mkdir -p $path
        cd $path
    fi

    if [[ "$dirname" == "" ]]; then
        dirname=${filename%.*}
        [[ "$filename" =~ (tar.gz|tar.bz2|tar.xz)$ ]] && dirname=${dirname%.*}
    fi

    if test -e $dirname; then
        if test -d $dirname; then
            cd $dirname
        fi
        return 0
    fi

    if test -f $filename; then
        if $binary; then
            return 0
        else
            decompress $filename
            if [[ $dirname == "" ]]; then
                rm $filename
            fi
        fi
    fi

    if test -e $dirname; then
        if test -d $dirname; then
            cd $dirname
        fi
        return 0
    fi

    if $git; then
        git clone --recursive --depth=1 $url
    elif command -v axel; then
        axel -n20 $url -o $filename
        if [ $? -ne 0 ]; then
            wget $url --no-check-certificate -O $filename
        fi
    else
        wget $url --no-check-certificate -O $filename
    fi

    if test -f $filename; then
        if $binary; then
            return 0
        else
            decompress $filename
            if [[ $dirname == "" ]]; then
                rm $filename
            fi
        fi
    fi

    if test -e $dirname; then
        if test -d $dirname; then
            cd $dirname
        fi
        return 0
    fi

    return 1
}

function install_env(){
    if ! grep C_INCLUDE_PATH $profile &>/dev/null; then
        echo "
export PYTHONPATH=$basepath/modules:\$PYTHONPATH
export PATH=$optpath/bin:$optpath/sbin:\$PATH
export LD_LIBRARY_PATH=$optpath/lib64:$optpath/lib:\$LD_LIBRARY_PATH
export LIBRARY_PATH=$optpath/lib64:$optpath/lib:\$LIBRARY_PATH
export C_INCLUDE_PATH=$optpath/include:\$C_INCLUDE_PATH
export CPLUS_INCLUDE_PATH=$optpath/include:\$CPLUS_INCLUDE_PATH
export PKG_CONFIG_PATH=$optpath/lib/pkgconfig:\$PKG_CONFIG_PATH
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export EDITOR=vim" >> $profile
        source $profile
    fi
}

function install_rm(){
    cp $basepath/script/bin/rm $optpath/bin/rm
}

function install_conf(){
    cp -r $confpath/pip ~/.pip
    cp -r $confpath/m2 ~/.m2
    cp -r $confpath/ssh ~/.ssh
    cp -r $confpath/npmrc ~/.npmrc
    cp -r $confpath/condarc ~/.condarc
    cp -r $confpath/mongorc.js ~/.mongorc.js
}

function install_ohmyzsh(){
    if ! command -v zsh; then
        install zsh
    fi

    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
    git clone https://github.com/zsh-users/zsh-autosuggestions.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
    # sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="ys"/g' ~/.zshrc
    # sed -i 's/^plugins=(\(.*\))/plugins=(\1 z extract wd zsh-syntax-highlighting zsh-autosuggestions)/g' ~/.zshrc
    # sed -i '61,63d' ~/.zshrc
    # sed -i '60a plugins=(git extract wd zsh-syntax-highlighting zsh-autosuggestions)' ~/.zshrc
    # echo 'setopt no_nomatch' >> ~/.zshrc
    cp $confpath/zshrc ~/.zshrc
}

function install_mac(){
    # 安装Xcode Command Line Tools
    xcode-select --install
    # 安装homebrew
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    # 替换homebrew:
    cd "$(brew --repo)"
    git remote set-url origin https://mirrors.ustc.edu.cn/brew.git
    # 替换homebrew-core
    cd "$(brew --repo)/Library/Taps/homebrew/homebrew-core"
    git remote set-url origin https://mirrors.ustc.edu.cn/homebrew-core.git
    # 替换homebrew-cask
    brew tap caskroom/homebrew-cask && brew install brew-cask-completion
    cd "$(brew --repo)"/Library/Taps/caskroom/homebrew-cask
    git remote set-url origin https://mirrors.ustc.edu.cn/homebrew-cask.git

    brew install cmake zsh wget axel ctags bat jq vim mosh htop httpie git coreutils python python3
    brew cask install iina
    brew install gnu-sed --with-default-names
    brew install gnu-tar --with-default-names
    brew install gnu-which --with-default-names
    brew install findutils --with-default-names

    sudo spctl --master-disable
    chsh -s /bin/zsh
    install_env
    install_ohmyzsh

    echo "
alias cls='tput reset'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'
alias l.='ls -d .* --color=auto'
alias ll='ls -l --color=auto'
alias ls='ls --color=auto'
export HOMEBREW_BOTTLE_DOMAIN=https://mirrors.ustc.edu.cn/homebrew-bottles
export PATH=\$(brew --prefix coreutils)/libexec/gnubin:\$PATH" >> ~/.zshrc

    source ~/.zshrc
    install_conf
    cd $tmppath
    download https://iterm2.com/downloads/stable/latest -f iterm2.zip -p /Applications
    download http://npm.taobao.org/mirrors/chromedriver/2.37/chromedriver_mac64.zip -p $optpath/bin -d chromedriver
    axel -n20 http://zipzapmac.com/DMGs/Go2Shell.dmg && open Go2Shell.dmg
}

function install_jdk(){
    download https://coding.net/u/zkdfbb/p/package/git/raw/master/jdk-8u131-linux-x64.tar.gz -p $runpath

    if ! grep JAVA_HOME $profile &>/dev/null; then
        echo "
export JAVA_HOME=`pwd`
export JRE_HOME=\$JAVA_HOME/jre
export CLASSPATH=\$JAVA_HOME/lib/dt.jar:\$JAVA_HOME/lib/tools.jar:\$JRE_HOME/lib
export PATH=\$JAVA_HOME/bin:\$JRE_HOME/bin:\$PATH" >> $profile
    fi
}

function install_hadoop(){
    download http://mirrors.tuna.tsinghua.edu.cn/apache/hadoop/common/hadoop-2.8.2/hadoop-2.8.2.tar.gz -p $runpath

    if ! grep HADOOP_HOME $profile &>/dev/null; then
        echo "
export HADOOP_HOME=`pwd`
export HADOOP_CONF_DIR=\$HADOOP_HOME/etc/hadoop
export YARN_HOME=\$HADOOP_HOME
export YARN_CONF_DIR=\$HADOOP_CONF_DIR
export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:\$HADOOP_HOME/lib/native
export PATH=\$HADOOP_HOME/bin:\$PATH" >> $profile
    fi
}

function install_pig(){
    download https://mirrors.tuna.tsinghua.edu.cn/apache/pig/pig-0.17.0/pig-0.17.0.tar.gz -p $runpath

    if ! grep PIG_HOME $profile &>/dev/null; then
        echo "
export PIG_HOME=`pwd`
export PIG_CLASSPATH=\$HADOOP_CONF_DIR
export PATH=\$PIG_HOME/bin:\$PATH" >> $profile
    fi
}

function install_spark(){
    download https://mirrors.tuna.tsinghua.edu.cn/apache/spark/spark-2.1.1/spark-2.1.1-bin-hadoop2.7.tgz -p $runpath

    if ! grep SPARK_HOME $profile &>/dev/null; then
        echo "
export SPARK_HOME=`pwd`
export SPARK_LIBRARY_PATH=\$SPARK_HOME/classpath/emr/*:\$SPARK_HOME/classpath/emrfs/*:\$SPARK_HOME/lib/*\$
export PATH=\$SPARK_HOME/bin:\$PATH" >> $profile
    fi
}

function install_hbase(){
    download http://archive.apache.org/dist/hbase/0.98.24/hbase-0.98.24-hadoop2-bin.tar.gz -p $runpath

    if ! grep HBASE_HOME $profile &>/dev/null; then
    echo "
export HBASE_HOME=`pwd`
export PIG_CLASSPATH=\`\$HBASE_HOME/bin/hbase classpath\`:\$PIG_CLASSPATH
export HADOOP_CLASSPATH=\`\$HBASE_HOME/bin/hbase classpath\`:\$HADOOP_CLASSPATH
export PATH=\$HBASE_HOME/bin:\$PATH" >> $profile
    fi
}

function install_maven(){
    download http://archive.apache.org/dist/maven/maven-3/3.5.0/binaries/apache-maven-3.5.0-bin.tar.gz -p $runpath
    echo "export PATH=`pwd`/bin:\$PATH" >> $profile
}

function install_redis(){
    download "http://download.redis.io/redis-stable.tar.gz"
    mkdir -p $optpath/bin $runpath/redis
    make -j10 && cp src/redis-server src/redis-cli $optpath/bin
    cp redis.conf $runpath/redis
    sed -i 's/daemonize no/daemonize yes/g' $runpath/redis/redis.conf
    cd $runpath/redis
    $optpath/bin/redis-server $runpath/redis/redis.conf
}

function install_mongo(){
    download https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-amazon2-4.0.0.tgz
    mkdir -p $optpath/bin $runpath/mongo
    cp -r bin/*  $optpath/bin
    cp $confpath/mongod.yml $runpath/mongo/mongod.yml
    cd $runpath/mongo
    mkdir -p data logs
    if command -v numactl &>/dev/null; then
        numactl --interleave=all $optpath/bin/mongod -f $runpath/mongo/mongod.yml
    else
        $optpath/bin/mongod -f $runpath/mongo/mongod.yml
    fi

    # sudo echo 'never' > /sys/kernel/mm/transparent_hugepage/enabled
    # sudo echo 'never' > /sys/kernel/mm/transparent_hugepage/defrag
}

function install_gcc(){
    export LD_LIBRARY_PATH=""
    export LIBRARY_PATH=""
    export C_INCLUDE_PATH=""
    export CPLUS_INCLUDE_PATH=""
    download "http://gcc.skazkaforyou.com/releases/gcc-4.8.5/gcc-4.8.5.tar.gz"
    ./contrib/download_prerequisites
    mkdir -p gcc-build && cd gcc-build
    ../configure --prefix=$optpath --enable-checking=release --enable-languages=c,c++ --disable-multilib && make -j10 && make install && ln -s $optpath/bin/gcc $optpath/bin/cc

    # sudo cp x86_64-unknown-linux-gnu/libstdc++-v3/src/.libs/libstdc++.so.6.0.21 /usr/lib64/
    # sudo mv /usr/lib64/libstdc++.so.6 /usr/lib64/libstdc++.so.6.bak
    # sudo ln -s /usr/lib64/libstdc++.so.6.0.21 /usr/lib64/libstdc++.so.6
    # sudo ldconfig
}

function install_pyenv(){
    export PYENV_ROOT=$runpath/pyenv
    if ! grep PYENV_VIRTUALENV_DISABLE_PROMPT $profile &>/dev/null; then
        echo "export PYENV_ROOT=$runpath/pyenv" >> $profile
        echo "export PATH=$PYENV_ROOT/bin:\$PATH" >> $profile
        echo "export PYENV_VIRTUALENV_DISABLE_PROMPT=1" >> $profile
        echo "export PYTHON_CONFIGURE_OPTS=\"--enable-shared\"" >> $profile
    fi
    source $profile
    curl -L https://raw.githubusercontent.com/pyenv/pyenv-installer/master/bin/pyenv-installer | sh
    if ! grep "virtualenv-init" $profile &>/dev/null; then
        echo "eval \"\$(pyenv init -)\"" >> $profile
        echo "eval \"\$(pyenv virtualenv-init -)\"" >> $profile
    fi
    if command -v pyenv &>/dev/null; then
        source $profile
        export PYTHON_CONFIGURE_OPTS="--enable-shared"
        CFLAGS="-I $optpath/include" LDFLAGS="-L $optpath/lib" pyenv install 2.7.14
        pyenv global 2.7.14
        pyenv virtualenv 2.7.14 py2
        pyenv activate py2
        return 0
    else
        return 1
    fi
}

function install_supervisor(){
    mkdir -p $runpath/supervisor/logs
    mkdir -p $runpath/supervisor/proc
    mkdir -p $runpath/supervisor/conf.d

    if ! grep supervisorctl $profile &>/dev/null; then
        echo "export SUPERVISOR_HOME=$runpath/supervisor" >> $profile
        echo "alias supervisorctl='supervisorctl -c $runpath/supervisor/supervisord.ini'" >> $profile
    fi
    source $profile

    pip install git+https://github.com/Supervisor/supervisor.git

    cp $confpath/supervisord.ini $runpath/supervisor/
    cd $runpath/supervisor
    supervisord -c $runpath/supervisor/supervisord.ini
    # sudo chkconfig supervisord on
}

function install_nginx(){
    mkdir -p $runpath/upload
    download_path="${runpath//\//\\\/}\/upload"
    sed -i "s/download_path/$download_path/g" $optpath/nginx/conf/nginx.conf
    ps -ef | grep $USER | grep -v grep | grep nginx | awk '{print $2}' | xargs kill -9
    $optpath/nginx/sbin/nginx -c $optpath/nginx/conf/nginx.conf
}

function install_upload(){
    mkdir -p $runpath/upload
    cd $runpath/upload
    echo "upload command: curl -XPUT --data-binary @filename http://$ip:8000?filename"
    nohup python -m upload &>/dev/null &
    return $?
}

function getsections(){
    sed -n '/\[*\]/p' $1 | egrep -v '^#' | tr -d [] | tr -t '\n' ' '
}

function readini(){
    SECTION=$1
    KEY=$2
    CONFIG=$3
    # sed 匹配[$SECION]到下一个[之间的所有行, egrep 去除所有包含[]的行、空行以及以#开头的行
    keys=`sed -n '/\['$SECTION'\]/,/\[/p'  $CONFIG | egrep -v '^\[|^\s*$|#' | awk -F '=' '{ print $1 }' | tr -t '\n' ' '`
    if [[ "$keys" =~ $KEY ]]; then
        # sed -n '/\['$SECTION'\]/,/\[/p'  $CONFIG | egrep -v '^\[|^\s*$|#' | awk -F '=' -v key=$KEY '{ value=""; for(i=2;i<=NF;i++) value=value""$i"="; gsub(/^ *| *$/, "", $1); gsub(/^ *|[= ]*$/, "", value); if($1==key) print value}'
        sed -n '/\['$SECTION'\]/,/\[/p'  $CONFIG | egrep -v '^\[|^\s*$|#' | sed -n '/^\s*'$KEY'/p' | cut -d '=' -f 2- | sed 's/^[ \t]*//g'
    fi
}

function install(){
    package=$1
    if command -v install_$package &>/dev/null; then
        cmd=install_$package
    elif [[ "$sections" =~ $package ]]; then
        depends=`readini $package depends $config`
        if [[ "$depends" != "" ]]; then
            for depend in $depends
            do
                if ! install $depend; then
                    return 1
                fi
            done
            # 重新读取变量
            package=$1
        fi

        git=`readini $package git $config`
        url=`readini $package url $config`
        cmd=`readini $package cmd $config`
        runtime=`readini $package runtime $config`
        binary=`readini $package binary $config`
        binaries=`readini $package binaries $config`
        filename=`readini $package filename $config`
        dirname=`readini $package dirname $config`
        preprocess=`readini $package preprocess $config`
        postprocess=`readini $package postprocess $config`

        [ ! -z "$package" ] && echo "package=$package"
        [ ! -z "$url" ] && echo "url=$url"
        [ ! -z "$git" ] && echo "git=$git"
        [ ! -z "$cmd" ] && echo "cmd=$cmd"
        [ ! -z "$runtime" ] && echo "runtime=$runtime"
        [ ! -z "$binary" ] && echo "binary=$binary"
        [ ! -z "$binaries" ] && echo "binaries=$binaries"
        [ ! -z "$filename" ] && echo "filename=$filename"
        [ ! -z "$dirname" ] && echo "dirname=$dirname"
        [ ! -z "$preprocess" ] && echo "preprocess=$preprocess"
        [ ! -z "$postprocess" ] && echo "postprocess=$postprocess"

        if [[ "$cmd" == "" ]]; then
            cmd="./configure --prefix=$optpath && make -j10 && make install"
        fi
        if [[ "$git" != "" ]]; then
            url=$git
            params=$params" -g"
        fi
        if [[ "$runtime" == "true" ]]; then
            params=$params" -p $runpath"
        fi
        if [[ "$binary" == "true" ]]; then
            params=$params" -b"
        fi
        if [[ "$filename" != "" ]]; then
            params=$params" -f $filename"
        fi
        if [[ "$dirname" != "" ]]; then
            params=$params" -d $dirname"
        fi

        url=`printf "OS=$OS ARCH=$ARCH\ncat << EOF\n$url\nEOF" | bash`
        download "$url" $params
    else
        echo "$package configure is not found !!!"
        return 1
    fi

    if [[ $? -ne 0 ]]; then
        return 1
    fi

    if [[ "$preprocess" != "" ]]; then
        eval "$preprocess"
    fi

    if [[ $? -ne 0 ]]; then
        return 1
    fi

    if ls | grep -i makefile &>/dev/null; then
        make clean
    fi
    if [[ "$binary" != "true" ]] && [[ "$runtime" != "true" ]] && [ "$binaries" != "true" ]; then
        set -o pipefail; eval "$cmd" | tee -a $basepath/script/logs/${package}.log
    fi

    if [[ $? -ne 0 ]]; then
        return 1
    fi

    if [[ "$postprocess" != "" ]]; then
        eval "$postprocess"
    fi
}

function init(){
    install_env
    if [ $# -eq 0 ]; then
        packages=`readini common packages $config`
    else
        packages=$@
    fi
    sections=`getsections $config`
    for package in $packages
    do
        if install $package; then
            echo "$package install succeed" >> $basepath/script/logs/result.log
        else
            echo "$package install failed" >> $basepath/script/logs/result.log
        fi
    done
}

init $@
