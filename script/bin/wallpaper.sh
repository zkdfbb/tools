#!/usr/bin/env bash
# -*- coding:utf-8 -*-

##############################################
#
#  Author: zhangkai
#  Email: zhangkai@cmcm.com
#  Last modified: 2017-10-06 18:43:18
#
##############################################

#提取壁纸图片URL
url=$(expr "$(curl -sSL http://cn.bing.com/?mkt=zh-CN |grep g_img)" : ".*g_img={url: \"\(.*\)\",id.*")
#去除url中的斜杠“\”
url="http://cn.bing.com${url//\\/}"
#提取图片名称
filename=$(expr "$url" : ".*/\(.*\)")
#本地图片地址-当前用户下缺省图片目录
localpath="/Users/$USER/Pictures/wallpaper/$filename"
#下载图片至本地
curl -ssL -o $localpath $url
#调用Finder应用切换桌面壁纸
# osascript -e "tell application \"Finder\" to set desktop picture to POSIX file \"$localpath\""
