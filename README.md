### Linux Toolbox

This toolbox is used for installation without root privileges.

For example, if you want to install anaconda3 and vim, go into the script folder, and run command:

```
./install.sh anaconda3
```

### Attention

1. The default anaconda3 installation folder is `~/.anaconda3` or `~/.anaconda2`. If you want to use another folder, you should run `export ANACONDA=/data/anaconda` before installation.

2. vim plugins need python & python3 support, you should install python & python3 manually, do not use anaconda3, `./install.sh python python3 vim`. You can install anaconda3 after that.

3. A simple http server (like http.server with upload and download supported) is integrated. You can run `python -m upload` to start it. If you want to enable demo or chart, You should install mongodb(`./install.sh mongo`).
